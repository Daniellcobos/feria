from django.contrib import admin

from .models import *

admin.site.register(Tienda)
admin.site.register(Producto)
admin.site.register(Cliente)
admin.site.register(Canasta)
