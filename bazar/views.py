
from .models import *
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.contrib.auth.models import User
from django.views import generic
from django.utils import timezone
from django.http import *
from rest_framework import viewsets
from .serializers import *
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
import requests
import json

import json
# Create your views here.
class IndexView(generic.ListView):
    template_name = 'bazar/index.html'
    context_object_name = 'latest_tienda_list'

    def get_queryset(self):
        """Tiendas en orden de creacion"""
        return Tienda.objects.all()


class DetailView(generic.DetailView):
    model = Tienda
    template_name = 'bazar/details.html'

class Detail2View(generic.DetailView):
    model = Producto
    template_name = 'bazar/details2.html'
class Detail3View(generic.DetailView):
    model = Cliente
    template_name = 'bazar/details3.html'


def addtocanasta(request,pk):
    p= Producto.objects.get(id=pk);
    n= request.user.id;
    c= Cliente.objects.get(usuario_id=n);
    c.save();

    try:
     C1=Canasta.objects.get(cliente=c);
     C1.productos.add(p);
     C1.cantidadp += 1;
     C1.save();
    except Canasta.DoesNotExist:
     C1= Canasta();
     C1.cliente = c;
     C1.save();
     C1.productos.add(p);
     C1.cantidadp += 1;
     C1.save();
    return render(request,'bazar/addc.html')
def comprar(request,pk):
    try:
     num= pk
     c1=Cliente.objects.get(id=num);
     c2=Canasta.objects.get(cliente=c1);
     price = 0;
     contad =c2.cantidadp;
     listap = []
     for producto in c2.productos.all():
        pri=producto.precio;
        prip=str(pri)
        contad= contad -1
        listap=listap + ["nombre:"+ producto.nombre,"precio:" +prip]
        price = price+pri;

     else:
      c1.cupo=c1.cupo-price;
      if c1.cupo > -1:
       c=c1.nombre;
       c1.save();
       recibo ={
       "Comprador" : c,
       "productos" : listap,
       }
       c2.delete();

       return JsonResponse(recibo)
      else:
       return HttpResponse('<h1>Usted es una especie de pobre diablo</h1>')

    except Canasta.DoesNotExist:
     raise Http404("Usted es imbecil, no tiene nada")
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class ProductViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer
class tiendaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Tienda.objects.all()
    serializer_class = TiendaSerializer
class compraapi(APIView):
  def post(self, request, pk):
   pid= pk
   url= ("http://localhost:8000/producto/{}".format(pid))
   r= request.get()
   jsan1= r.json()
   precio=jsan1["precio"]
   n= request.user.id;
   c= Cliente.objects.get(usuario_id=n);
