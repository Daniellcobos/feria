from django.urls import path
from django.conf.urls import url, include
from django.contrib import admin
from .resources import productResource
from . import views

app_name = 'bazar'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('tienda/<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/', views.Detail2View.as_view(), name='detail2'),
    path('addcanasta/<int:pk>/', views.addtocanasta, name='addc'),
    path('cliente/<int:pk>/', views.Detail3View.as_view(), name='cliento'),
    path('cliente/<int:pk>/canasta/', views.comprar, name='compre'),


]
