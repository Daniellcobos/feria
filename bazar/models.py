from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
class Tienda(models.Model):
    nombre_tienda = models.CharField(max_length=200)
    def __str__(self):
        return self.nombre_tienda

class Producto(models.Model):

    nombre= models.CharField(max_length=200)
    precio = models.IntegerField(default=0)
    def __str__(self):
        return str(self.id)

class Cliente(models.Model):
    usuario_id = models.OneToOneField(User, on_delete=models.CASCADE, default = 0)

    nombre= models.CharField(max_length=200)
    cupo= models.IntegerField(default=0)
    @receiver(post_save, sender=User)
    def create_user_cliente(sender, instance, created, **kwargs):
     if created:
      Cliente.objects.create(usuario_id=instance)

      @receiver(post_save, sender=User)
      def save_user_cliente(sender, instance, **kwargs):
       instance.Cliente.save()
    def __str__(self):
        return self.nombre

class Canasta(models.Model):
    cliente = models.ForeignKey(Cliente,on_delete=models.CASCADE)
    productos = models.ManyToManyField(Producto)
    cantidadp = models.IntegerField(default=0);
