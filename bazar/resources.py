# api/resources.py
from tastypie.resources import ModelResource
from .models import *
class productResource(ModelResource):
    class Meta:
        queryset = Producto.objects.all()
        resource_name = 'producto'
class ClientResource(ModelResource):
    class Meta:
        queryset = Cliente.objects.all()
        resource_name = 'cliente'
class TiendaResource(ModelResource):
    class Meta:
        queryset = Tienda.objects.all()
        resource_name = 'tienda'
